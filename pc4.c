#include <stdio.h>
#include <stdlib.h>

#define MAX_K 750

struct pair {
	int a, b;
};

struct pqueue {
	size_t size;
	struct pair *elems[MAX_K];
};

/* array of k min sums so far */
int sums[MAX_K];
/* current array being read */
int current[MAX_K];

void process_arrays(int k);
int pair_sum(struct pair *p);

void pqueue_init(struct pqueue *p);
void pqueue_insert(struct pqueue *p, struct pair *pr);
struct pair *pqueue_extract_min(struct pqueue *p);
void pqueue_bubble_down(struct pqueue *p, unsigned pos);

void swap(struct pair **a, struct pair **b);
int paircmp(struct pair *a, struct pair *b);
int intcmp(const void *a, const void *b);

int main()
{
	int i, j, k;
	scanf("%d", &k);
	/* read first array into sums */
	for (i = 0; i < k; ++i)
		scanf("%d", &sums[i]);
	qsort(sums, k, sizeof(int), &intcmp);
	/* read subsequent arrays into current and compute min sums */
	for (i = 1; i < k; ++i) {
		for (j = 0; j < k; ++j)
			scanf("%d", &current[j]);
		qsort(current, k, sizeof(int), &intcmp);
		process_arrays(k);
	}
	for (i = 0; i < k; ++i) {
		printf("%d", sums[i]);
		if (i != k - 1)
			putchar(' ');
	}
	putchar('\n');
	return EXIT_SUCCESS;
}

/* process_arrays: finds the k minimum sums of sums and current */
void process_arrays(int k)
{
	int i, j;
	struct pqueue pq;
	pqueue_init(&pq);
	int s[MAX_K];
	struct pair pairs[MAX_K * 2];
	int npairs = 0;
	pairs[0].a = pairs[0].b = 0;
	pqueue_insert(&pq, &pairs[npairs++]);
	/* matrix of all index pairs to avoid checking duplicates */
	int checked[MAX_K][MAX_K];
	for (i = 0; i < k; ++i) {
		for (j = 0; j < k; ++j)
			checked[i][j] = 0;
	}
	for (i = 0; i < k; ++i) {
		struct pair *min = pqueue_extract_min(&pq);
		if (min->b < k) {
			pairs[npairs].a = min->a;
			pairs[npairs].b = min->b + 1;
			if (!checked[pairs[npairs].a][pairs[npairs].b]) {
				checked[pairs[npairs].a][pairs[npairs].b] = 1;
				pqueue_insert(&pq, &pairs[npairs++]);
			}
		}
		if (min->a < k) {
			pairs[npairs].a = min->a + 1;
			pairs[npairs].b = min->b;
			if (!checked[pairs[npairs].a][pairs[npairs].b]) {
				checked[pairs[npairs].a][pairs[npairs].b] = 1;
				pqueue_insert(&pq, &pairs[npairs++]);
			}
		}
		s[i] = pair_sum(min);
	}
	/* update sums */
	for (i = 0; i < k; ++i)
		sums[i] = s[i];
}

/* pair_sum: find the sum of the array elements at the pair's indicies */
int pair_sum(struct pair *p)
{
	return sums[p->a] + current[p->b];
}

/* pqueue_init: initialize a priority queue */
void pqueue_init(struct pqueue *p)
{
	p->size = 0;
}

/* pqueue_insert: insert an item into a priority queue */
void pqueue_insert(struct pqueue *p, struct pair *pr)
{
	int i, j;
	i = p->size++;
	p->elems[i] = pr;
	while (i > 0 && paircmp(p->elems[i], p->elems[(j = i / 2)]) < 0) {
		/* bubble up */
		swap(&p->elems[i], &p->elems[j]);
		i /= 2;
	}
}

/* pqueue_extract_min: remove and return the minimum element */
struct pair *pqueue_extract_min(struct pqueue *p)
{
	unsigned i = 0;
	struct pair *val = p->elems[i];
	swap(&p->elems[i], &p->elems[--(p->size)]);
	pqueue_bubble_down(p, i);
	return val;
}

/* pqueue_bubble_down: bubble element down to correct position */
void pqueue_bubble_down(struct pqueue *p, unsigned pos)
{
	/* index of left and right child */
	unsigned left = (pos == 0 ? 1 : pos * 2);
	unsigned right = (pos == 0 ? 2 : pos * 2 + 1);
	unsigned min = pos;

	if (left < p->size && paircmp(p->elems[left], p->elems[min]) < 0)
		min = left;
	if (right < p->size && paircmp(p->elems[right], p->elems[min]) < 0)
		min = right;
	
	if (min != pos) {
		swap(&p->elems[pos], &p->elems[min]);
		pqueue_bubble_down(p, min);
	}
}

/* swap: swap two pairs */
void swap(struct pair **a, struct pair **b)
{
	struct pair *tmp = *a;
	*a = *b;
	*b = tmp;
}

/* paircmp: compare two pairs by value of sum of array elements */
int paircmp(struct pair *a, struct pair *b)
{
	return pair_sum(a) - pair_sum(b);
}

/* intcmp: compare two ints */
int intcmp(const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}
