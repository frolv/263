#include <stdio.h>
#include <stdlib.h>

#define MAX_PEOPLE 10000

struct person {
	int rank;
	struct person *p;
	int balance, done;
};

/* array of all people */
struct person people[MAX_PEOPLE];

void initialize(int npeople, int nfriendships);
struct person *find_set(struct person *p);
void link(struct person *a, struct person *b);
int check_balances(int npeople);

int main()
{
	int i, ncases;
	scanf("%d", &ncases);
	for (i = 0; i < ncases; ++i) {
		int npeople, nfriendships;
		scanf("%d %d", &npeople, &nfriendships);
		initialize(npeople, nfriendships);
		printf("%sPOSSIBLE\n", check_balances(npeople) ? "" : "IM");
	}
	return EXIT_SUCCESS;
}

/* initialize: create nodes for all people and join according to friendships */
void initialize(int npeople, int nfriendships)
{
	int i;
	for (i = 0; i < npeople; ++i) {
		scanf("%d", &people[i].balance);
		people[i].rank = 0;
		people[i].p = &people[i];
		people[i].done = 0;
	}
	for (i = 0; i < nfriendships; ++i) {
		int a, b;
		scanf("%d %d", &a, &b);
		struct person *as, *bs;
		/* unionize both sets if different */
		if ((as = find_set(&people[a])) != (bs = find_set(&people[b])))
			link(as, bs);
	}
}

/* find_set: find the representative of a set */
struct person *find_set(struct person *p)
{
	if (p->p != p)
		p->p = find_set(p->p);
	return p->p;
}

/* link: join two sets and update total balance */
void link(struct person *a, struct person *b)
{
	if (a->rank > b->rank) {
		b->p = a;
		a->balance += b->balance;
	} else {
		a->p = b;
		b->balance += a->balance;
		if (a->rank == b->rank)
			b->rank++;
	}
}

/* check_balances: check if the balance of any group is non-zero */
int check_balances(int npeople)
{
	int i;
	for (i = 0; i < npeople; ++i) {
		struct person *rep;
		/* only check each set once */
		if (!(rep = find_set(&people[i]))->done) {
			rep->done = 1;
			if (rep->balance)
				return 0;
		}
	}
	return 1;
}
