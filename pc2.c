#include <stdio.h>
#include <stdlib.h>

#define MAX_PACKAGES 100

int npackages;

/* adjacency matrix for package graph */
int packages[MAX_PACKAGES][MAX_PACKAGES];

struct list {
	int key;
	struct list *next;
};

/* list of packages in order */
struct list *head;
struct list *tail;

void build_graph(int nrules);
void topsort();
int is_dependent(int id);
void insert_sorted(struct list **ll, int key);
int remove_front(struct list **ll);

int main()
{
	head = NULL;
	tail = NULL;
	int nrules;
	scanf("%d %d", &npackages, &nrules);
	build_graph(nrules);
	topsort();
	struct list *l;
	for (l = head; l; l = l->next) {
		printf("%d", l->key);
		if (l->next)
			putchar(' ');
	}
	printf("\n");
	return EXIT_SUCCESS;
}

/* build_graph: create a graph of all packages and dependencies */
void build_graph(int nrules)
{
	int i;
	for (i = 0; i < npackages; ++i) {
		int j;
		for (j = 0; j < npackages; ++j)
			packages[i][j] = 0;
	}
	for (i = 0; i < nrules; ++i) {
		int id, ndeps;
		scanf("%d %d", &id, &ndeps);
		while (ndeps--) {
			int p;
			scanf("%d", &p);
			packages[p - 1][id - 1] = 1;
		}
	}
}

/* topsort: topologically sort the packages */
void topsort()
{
	struct list *sl = NULL;
	int i;
	for (i = 0; i < npackages; ++i) {
		/* add all packages not depended on by others to list */
		if (!is_dependent(i))
			insert_sorted(&sl, i);
	}
	while (sl) {
		/* sl is sorted so gets lowest independent packages first */
		int key = remove_front(&sl);
		struct list *l;
		if (!(l = malloc(sizeof(struct list)))) {
			fprintf(stderr, "error: out of memory\n");
			exit(1);
		}
		l->key = key + 1;
		if (!head) {
			head = l;
			tail = l;
		} else {
			tail->next = l;
			tail = l;
		}
		for (i = 0; i < npackages; ++i) {
			if (packages[key][i]) {
				/* remove edge from graph */
				packages[key][i] = 0;
				if (!is_dependent(i))
					insert_sorted(&sl, i);
			}
		}
	}
}

/* is_dependent: check if a package is depended on by any others */
int is_dependent(int id)
{
	int i;
	for (i = 0; i < npackages; ++i) {
		if (packages[i][id])
			return 1;
	}
	return 0;
}

/* insert_sorted: insert a key to a list in sorted order */
void insert_sorted(struct list **ll, int key)
{
	struct list *m;
	if (!(m = malloc(sizeof(struct list)))) {
		fprintf(stderr, "error: out of memory\n");
		exit(1);
	}
	m->key = key;
	for (; *ll && (*ll)->key < key; ll = &(*ll)->next)
		;
	m->next = *ll;
	*ll = m;
}

/* remove_front: remove and return first element in a list */
int remove_front(struct list **ll)
{
	struct list *tmp = *ll;
	int key = tmp->key;
	*ll = tmp->next;
	free(tmp);
	return key;
}
