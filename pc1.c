#include <stdio.h>
#include <stdlib.h>

#define MAX_FLOORS 5000000
#define WHITE 0
#define GRAY 1

/* total number of floors */
int nfloors;

/* amount by which you can travel */
int up, down;

/* target floor */
int target;

/* graph node representing a single floor */
struct node {
	int dest[2];
	int colour, dist;
};

/* graph of all floors */
struct node floors[MAX_FLOORS];

struct list {
	int key;
	struct list *next;
};

struct queue {
	size_t size;
	struct list *head, *tail;
};

void build_graph();
void bfs(int start);

void init_queue(struct queue *q);
void free_queue(struct queue *q);
void enqueue(struct queue *q, int n);
int dequeue(struct queue *q);

int main()
{
	int start;
	scanf("%d %d %d %d %d", &nfloors, &start, &target, &up, &down);
	target--;
	start--;
	build_graph();
	bfs(start);
	int result = floors[target].dist;
	printf("%d\n", result == -1 ? 0 : result);
	return EXIT_SUCCESS;
}

/* build_graph: populate floors graph */
void build_graph()
{
	int i;
	for (i = 0; i < nfloors; ++i) {
		struct node f;
		int floorup = i + up;
		int floordown = i - down;
		f.dest[0] = floorup < nfloors ? floorup : i;
		f.dest[1] = floordown >= 0 ? floordown : i;
		f.colour = WHITE;
		f.dist = -1;
		floors[i] = f;
	}
}

/* bfs: perform a breadth first search on floors */
void bfs(int start)
{
	floors[start].colour = GRAY;
	floors[start].dist = 0;
	struct queue q;
	init_queue(&q);
	enqueue(&q, start);
	while (q.size) {
		int i;
		int floor = dequeue(&q);
		for (i = 0; i < 2; ++i) {
			int f = floors[floor].dest[i];
			if (floors[f].colour == WHITE) {
				floors[f].colour = GRAY;
				floors[f].dist = floors[floor].dist + 1;
				if (f == target) {
					free_queue(&q);
					return;
				}
				enqueue(&q, f);
			}
		}
	}
}

/* init_queue: create an empty queue */
void init_queue(struct queue *q)
{
	q->head = NULL;
	q->tail = NULL;
	q->size = 0;
}

/* free_queue: free all remaining items in queue */
void free_queue(struct queue *q)
{
	while (q->size)
		dequeue(q);
}

/* enqueue: add an item to a queue */
void enqueue(struct queue *q, int n)
{
	struct list *l = malloc(sizeof(struct list));
	l->key = n;
	l->next = NULL;
	q->size++;
	if (!q->head) {
		q->head = l;
		q->tail = l;
	} else {
		q->tail->next = l;
		q->tail = l;
	}
}

/* dequeue: remove an item from a queue */
int dequeue(struct queue *q)
{
	struct list *tmp = q->head;
	int n = tmp->key;
	q->head = tmp->next;
	q->size--;
	free(tmp);
	return n;
}
