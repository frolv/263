CC=gcc
CFLAGS=-Wall -Wextra -O2 -c

all: pc1 pc2 pc3 pc4

pc1: pc1.o
	$(CC) -o pc1 pc1.o

pc2: pc2.o
	$(CC) -o pc2 pc2.o

pc3: pc3.o
	$(CC) -o pc3 pc3.o

pc4: pc4.o
	$(CC) -o pc4 pc4.o

pc1.o: pc1.c
	$(CC) $(CFLAGS) pc1.c
	
pc2.o: pc2.c
	$(CC) $(CFLAGS) pc2.c

pc3.o: pc3.c
	$(CC) $(CFLAGS) pc3.c

pc4.o: pc4.c
	$(CC) $(CFLAGS) pc4.c

clean:
	rm -f pc1 pc2 pc3 pc4 *.o
